FROM node:latest

WORKDIR /usr/src/app

COPY . /usr/src/app

RUN yarn install
RUN yarn run build

CMD ["npm", "run", "prod"]
