import { configureStore } from "@reduxjs/toolkit";
import rootReducer from "./reducers";
import { Provider } from "react-redux";
import PatientsPage from "./features/patients/PatientsPage";

import "semantic-ui-css/semantic.min.css";
import "./App.css";

const store = configureStore({ reducer: rootReducer });

function App() {
  return (
    <Provider store={store}>
      <PatientsPage />
    </Provider>
  );
}

export default App;
