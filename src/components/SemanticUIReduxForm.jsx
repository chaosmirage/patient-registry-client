import { Form } from "semantic-ui-react";

export const Checkbox = (field) => (
  <Form.Checkbox
    checked={!!field.input.value}
    name={field.input.name}
    label={field.label}
    onChange={(e, { checked }) => field.input.onChange(checked)}
  />
);

export const Radio = (field) => (
  <Form.Radio
    checked={field.input.value === field.radioValue}
    label={field.label}
    name={field.input.name}
    onChange={(e, { checked }) => field.input.onChange(field.radioValue)}
  />
);

export const Select = (field) => (
  <Form.Select
    label={field.label}
    name={field.input.name}
    onChange={(e, { value }) => field.input.onChange(value)}
    options={field.options}
    placeholder={field.placeholder}
    value={field.input.value}
    required={field.required}
  />
);

export const TextArea = (field) => (
  <Form.TextArea
    {...field.input}
    label={field.label}
    placeholder={field.placeholder}
    required={field.required}
  />
);
