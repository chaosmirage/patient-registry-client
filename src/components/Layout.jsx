import { Container, Menu } from "semantic-ui-react";
import styled from "styled-components";

const Content = styled.div`
  height: calc(100vh - 56px);
`;

const Layout = ({ children }) => (
  <>
    <Menu inverted style={{ borderRadius: 0 }}>
      <Container>
        <Menu.Item as="a" href="/" header>
          Patient registry
        </Menu.Item>
      </Container>
    </Menu>

    <Content>{children}</Content>
  </>
);

export default Layout;
