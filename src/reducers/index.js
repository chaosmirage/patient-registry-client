import { combineReducers } from "redux";
import patientsSlice from "../features/patients/patientsSlice";
import { reducer as formReducer } from "redux-form";

export default combineReducers({
  patients: patientsSlice,
  form: formReducer,
});
