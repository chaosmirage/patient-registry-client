import axios from "axios";
import { API_HOST } from "../constants";

const url = `${API_HOST}/api/patients`;

export async function getPatients() {
  try {
    const { data } = await axios.get(url);

    return data;
  } catch (err) {
    throw err;
  }
}

export async function postPatients(patient) {
  try {
    const { data } = await axios.post(url, patient);

    return data;
  } catch (err) {
    throw err;
  }
}

export async function updatePatients(id, patient) {
  try {
    const { data } = await axios.put(`${url}/${id}`, patient);

    return data;
  } catch (err) {
    throw err;
  }
}

export async function deletePatients(id) {
  try {
    const { data } = await axios.delete(`${url}/${id}`);

    return data;
  } catch (err) {
    throw err;
  }
}
