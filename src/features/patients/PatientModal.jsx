import { useCallback } from "react";
import { useDispatch } from "react-redux";
import { Button, Modal } from "semantic-ui-react";
import { submit } from "redux-form";
import PatientForm, { FORM_NAME } from "./PatientForm";

const processNameTypeToModalHeader = {
  patientCreate: "Создание нового пациента",
  patientUpdate: "Редактирование пациента",
};

const processNameTypeToOkButtonText = {
  patientCreate: "Создать",
  patientUpdate: "Обновить",
};

const PatientModal = ({ onClose, onOk, isSubmitting, processName }) => {
  const dispatch = useDispatch();

  const submitForm = useCallback(() => dispatch(submit(FORM_NAME)), [dispatch]);

  return (
    <Modal open>
      <Modal.Header>{processNameTypeToModalHeader[processName]}</Modal.Header>
      <Modal.Content scrolling>
        <PatientForm onSubmit={onOk} />
      </Modal.Content>
      <Modal.Actions>
        <Button onClick={onClose}>Закрыть</Button>
        <Button onClick={submitForm} primary loading={isSubmitting}>
          {processNameTypeToOkButtonText[processName]}
        </Button>
      </Modal.Actions>
    </Modal>
  );
};

export default PatientModal;
