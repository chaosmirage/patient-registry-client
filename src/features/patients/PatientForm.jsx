import React from "react";
import { Field, reduxForm } from "redux-form";
import { Form, Message } from "semantic-ui-react";
import { Select, TextArea } from "../../components/SemanticUIReduxForm";

export const FORM_NAME = "patientForm";

const PatientForm = ({ handleSubmit, error }) => {
  return (
    <Form onSubmit={handleSubmit}>
      {error && <Message color="red">{error}</Message>}
      <Field
        required
        component={Form.Input}
        label="ФИО"
        name="full_name"
        placeholder="Введите ФИО"
      />
      <Field
        required
        component={Form.Input}
        label="ОМС"
        name="policy_number"
        placeholder="Введите ОМС"
      />
      <Field
        required
        component={Select}
        label="Пол"
        name="gender"
        options={[
          { key: "m", text: "Мужской", value: "m" },
          { key: "f", text: "Женский", value: "f" },
        ]}
        placeholder="Выберите пол"
      />
      <Field
        required
        component={Form.Input}
        label="Дата рождения"
        name="birthday"
        placeholder="Введите дату рождения"
      />
      <Field
        required
        component={TextArea}
        label="Адрес"
        name="address"
        placeholder="Введите адрес"
      />
    </Form>
  );
};

export default reduxForm({
  form: FORM_NAME,
})(PatientForm);
