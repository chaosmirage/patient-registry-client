import React from "react";
import { rest } from "msw";
import { setupServer } from "msw/node";
import { render, screen, waitFor, fireEvent } from "../../test-utils";
import "@testing-library/jest-dom/extend-expect";
import PatientsPage from "./PatientsPage";

import { API_HOST } from "../../constants";

const url = `${API_HOST}/api/patients`;

const PATIENTS_FIXTURE = [
  {
    id: 1,
    full_name: "Vasya",
    birthday: "1990-02-02",
    address: "Ул. Пушкина, д.1",
    policy_number: "1234123412341234",
    gender: "m",
    createdAt: "2020-12-09T16:58:19.592Z",
    updatedAt: "2020-12-09T16:58:19.592Z",
  },
  {
    id: 2,
    full_name: "Petya",
    birthday: "1990-01-01",
    address: "Ул. Пушкина, д.2",
    policy_number: "1234123412341237",
    gender: "m",
    createdAt: "2020-12-09T16:58:19.592Z",
    updatedAt: "2020-12-09T16:58:19.592Z",
  },
];

const server = setupServer(
  rest.get(url, (req, res, ctx) => {
    return res(ctx.json(PATIENTS_FIXTURE));
  })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test("Render patient list", async () => {
  render(<PatientsPage />);

  await waitFor(() => screen.getByTestId(`patient-${PATIENTS_FIXTURE[0].id}`));
  await waitFor(() => screen.getByTestId(`patient-${PATIENTS_FIXTURE[1].id}`));

  expect(
    screen.getByTestId(`patient-${PATIENTS_FIXTURE[0].id}`)
  ).toHaveTextContent(PATIENTS_FIXTURE[0].full_name);
});

test("Patient list search works", async () => {
  render(<PatientsPage />);

  const searchInput = await waitFor(() =>
    screen.getByTestId("patients-search")
  );

  fireEvent.change(searchInput, {
    target: { value: PATIENTS_FIXTURE[1].full_name },
  });

  expect(
    screen.getByTestId(`patient-${PATIENTS_FIXTURE[1].id}`)
  ).toHaveTextContent(PATIENTS_FIXTURE[1].full_name);

  fireEvent.change(searchInput, {
    target: { value: "unexpected value" },
  });

  screen.getByTestId(`patients-search-not-found-message`);

  fireEvent.change(searchInput, {
    target: { value: "" },
  });

  await waitFor(() => screen.getByTestId(`patient-${PATIENTS_FIXTURE[0].id}`));
  await waitFor(() => screen.getByTestId(`patient-${PATIENTS_FIXTURE[1].id}`));
});
