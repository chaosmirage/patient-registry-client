import { useState, useCallback, useMemo } from "react";
import _ from "lodash";
import { Table, Button, Icon, Confirm, Input } from "semantic-ui-react";

const VALUE_TO_TEXT = {
  m: "Мужской",
  f: "Женский",
};

const FILTER_INITIAL_STATE = {
  processState: "idle",
  value: "",
  results: null,
};

const PatientList = ({ data, onCreate, onDelete, onUpdate }) => {
  const [activePatientId, changeActivePatientId] = useState(null);
  const [showConfirm, changeShowConfirmState] = useState(false);

  const [filter, changeFilter] = useState(FILTER_INITIAL_STATE);

  const closeConfirm = useCallback(() => {
    changeShowConfirmState(false);
  }, []);

  const openConfirm = useCallback(
    (patientId) => () => {
      changeActivePatientId(patientId);
      changeShowConfirmState(true);
    },
    []
  );

  const handleDeletePatient = useCallback(async () => {
    onDelete(activePatientId);
    changeActivePatientId(null);
  }, [activePatientId, changeActivePatientId, onDelete]);

  const showEmptyData = useMemo(() => {
    return _.isEmpty(data);
  }, [data]);

  const handleChangeFilter = useCallback(
    (e, { value: filterInputValue }) => {
      if (filterInputValue === "") {
        changeFilter(FILTER_INITIAL_STATE);
        return;
      }

      const filtered = data.filter((item) => {
        return Object.values(item)
          .join("")
          .toLowerCase()
          .trim()
          .includes(filterInputValue.toLowerCase().trim());
      });

      const filteredToNewState = [
        {
          check: () => !_.isEmpty(filtered),
          getNewState: () => {
            return {
              value: filterInputValue,
              results: filtered,
              processState: "resultsFound",
            };
          },
        },
        {
          check: () => _.isEmpty(filtered),
          getNewState: () => {
            return {
              value: filterInputValue,
              results: null,
              processState: "noMatches",
            };
          },
        },
        {
          check: () => true,
          getNewState: () => {
            return FILTER_INITIAL_STATE;
          },
        },
      ];

      const { getNewState } = filteredToNewState.find(({ check }) => check());

      changeFilter(getNewState());
    },
    [data]
  );

  const preparedData =
    filter.processState === "resultsFound" ? filter.results : data;

  const showNotFoundPatients = filter.processState === "noMatches";

  const showPatientsList = !showEmptyData && !showNotFoundPatients;

  return (
    <>
      <Button primary onClick={onCreate} style={{ marginBottom: 8 }}>
        Создать
      </Button>

      <Input
        fluid
        input={{ "data-testid": "patients-search" }}
        icon="search"
        placeholder="Введите подстроку для поиска"
        onChange={handleChangeFilter}
        value={filter.value}
      />

      {showEmptyData && (
        <div style={{ paddingTop: 8 }}>Нет созданных пациентов</div>
      )}

      {showNotFoundPatients && (
        <div
          style={{ paddingTop: 8 }}
          data-testid="patients-search-not-found-message"
        >
          Не найдено результатов
        </div>
      )}

      {showPatientsList && (
        <>
          <Table>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Имя</Table.HeaderCell>
                <Table.HeaderCell>Дата рождения</Table.HeaderCell>
                <Table.HeaderCell>Адрес</Table.HeaderCell>
                <Table.HeaderCell>Пол</Table.HeaderCell>
                <Table.HeaderCell>Полис ОМС</Table.HeaderCell>
                <Table.HeaderCell>Действия</Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              {preparedData.map(
                (
                  { id, full_name, gender, birthday, policy_number, address },
                  index
                ) => {
                  return (
                    <Table.Row key={id} data-testid={`patient-${id}`}>
                      <Table.Cell>{full_name}</Table.Cell>
                      <Table.Cell>{birthday}</Table.Cell>
                      <Table.Cell>{address}</Table.Cell>
                      <Table.Cell>{VALUE_TO_TEXT[gender]}</Table.Cell>
                      <Table.Cell>{policy_number}</Table.Cell>
                      <Table.Cell>
                        <Button.Group icon>
                          <Button onClick={onUpdate(data[index])}>
                            <Icon name="pencil" />
                          </Button>
                          <Button onClick={openConfirm(id)}>
                            <Icon name="trash" />
                          </Button>
                        </Button.Group>
                      </Table.Cell>
                    </Table.Row>
                  );
                }
              )}
            </Table.Body>
          </Table>
          <Confirm
            open={showConfirm}
            onCancel={closeConfirm}
            onConfirm={handleDeletePatient}
            content={"Вы действительно хотите удалить пацента?"}
            cancelButton={"Отменить"}
            confirmButton={"Удалить"}
          />
        </>
      )}
    </>
  );
};

export default PatientList;
