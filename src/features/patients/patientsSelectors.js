import { createSelector } from "@reduxjs/toolkit";
import _ from "lodash";
import { patientsSlice } from "./patientsSlice";

const getAllPatientsRequestState = (state) =>
  state[patientsSlice.name].allPatientsRequestState;

const getCreatePatientRequestState = (state) =>
  state[patientsSlice.name].createPatientRequestState;

const getDeletePatientRequestState = (state) =>
  state[patientsSlice.name].deletePatientRequestState;

const getUpdatePatientRequestState = (state) =>
  state[patientsSlice.name].updatePatientRequestState;

const getPatients = (state) => state[patientsSlice.name].data;

export const allPatientsSelector = createSelector(getPatients, _.identity);

export const allPatientsRequestStateSelector = createSelector(
  getAllPatientsRequestState,
  _.identity
);

export const createPatientRequestStateSelector = createSelector(
  getCreatePatientRequestState,
  _.identity
);

export const deletePatientRequestStateSelector = createSelector(
  getDeletePatientRequestState,
  _.identity
);

export const updatePatientRequestStateSelector = createSelector(
  getUpdatePatientRequestState,
  _.identity
);
