import { createSlice } from "@reduxjs/toolkit";
import {
  getPatients as getPatientsAPI,
  postPatients as postPatientsAPI,
  deletePatients as deletePatientsAPI,
  updatePatients as updatePatientsAPI,
} from "../../api/patientsAPI";

export const patientsSlice = createSlice({
  name: "patients",
  initialState: {
    data: [],
    allPatientsRequestState: "idle",
    createPatientRequestState: "idle",
    error: null,
  },
  reducers: {
    getPatientsRequested: (state, action) => {
      state.allPatientsRequestState = "requested";
      state.error = null;
    },
    getPatientsSuccess: (state, action) => {
      state.allPatientsRequestState = "success";
      state.data = action.payload;
    },
    getPatientsFailed: (state, action) => {
      state.allPatientsRequestState = "failed";
      state.error = action.payload;
      state.data = [];
    },
    createPatientRequested: (state, action) => {
      state.createPatientRequestState = "requested";
      state.error = null;
    },
    createPatientSuccess: (state, action) => {
      state.createPatientRequestState = "success";
    },
    createPatientFailed: (state, action) => {
      state.createPatientRequestState = "failed";
      state.error = action.payload;
    },
    deletePatientRequested: (state, action) => {
      state.deletePatientRequestState = "requested";
      state.error = null;
    },
    deletePatientSuccess: (state, action) => {
      state.deletePatientRequestState = "success";
    },
    deletePatientFailed: (state, action) => {
      state.deletePatientRequestState = "failed";
      state.error = action.payload;
    },
    updatePatientRequested: (state, action) => {
      state.updatePatientRequestState = "requested";
      state.error = null;
    },
    updatePatientSuccess: (state, action) => {
      state.updatePatientRequestState = "success";
    },
    updatePatientFailed: (state, action) => {
      state.updatePatientRequestState = "failed";
      state.error = action.payload;
    },
  },
});

export const {
  getPatientsSuccess,
  getPatientsRequested,
  getPatientsFailed,
  createPatientRequested,
  createPatientSuccess,
  createPatientFailed,
  deletePatientRequested,
  deletePatientSuccess,
  deletePatientFailed,
  updatePatientRequested,
  updatePatientSuccess,
  updatePatientFailed,
} = patientsSlice.actions;

export default patientsSlice.reducer;

export const getPatients = () => async (dispatch) => {
  dispatch(getPatientsRequested());
  try {
    const patients = await getPatientsAPI();
    dispatch(getPatientsSuccess(patients));
  } catch (err) {
    dispatch(getPatientsFailed(err.toString()));
    return Promise.reject(err);
  }
};

export const createPatient = (patient) => async (dispatch) => {
  dispatch(createPatientRequested());
  try {
    const createdPatient = await postPatientsAPI(patient);
    dispatch(createPatientSuccess());
    return createdPatient;
  } catch (err) {
    dispatch(createPatientFailed(err.toString()));
    return Promise.reject(err);
  }
};

export const updatePatient = (id, patient) => async (dispatch) => {
  dispatch(updatePatientRequested());
  try {
    const deletedPatient = await updatePatientsAPI(id, patient);
    dispatch(updatePatientSuccess());
    return deletedPatient;
  } catch (err) {
    dispatch(updatePatientFailed(err.toString()));
    return Promise.reject(err);
  }
};

export const deletePatient = (id) => async (dispatch) => {
  dispatch(deletePatientRequested());
  try {
    const deletedPatient = await deletePatientsAPI(id);
    dispatch(deletePatientSuccess());
    return deletedPatient;
  } catch (err) {
    dispatch(deletePatientFailed(err.toString()));
    return Promise.reject(err);
  }
};
