import { useCallback, useEffect, useState, useMemo } from "react";
import _ from "lodash";
import { useSelector, useDispatch } from "react-redux";
import { initialize, SubmissionError } from "redux-form";
import { Container } from "semantic-ui-react";
import Layout from "../../components/Layout";
import PatientList from "./PatientList";
import PatientModal from "./PatientModal";
import {
  getPatients,
  createPatient,
  deletePatient,
  updatePatient,
} from "./patientsSlice";
import {
  allPatientsSelector,
  allPatientsRequestStateSelector,
  createPatientRequestStateSelector,
  deletePatientRequestStateSelector,
  updatePatientRequestStateSelector,
} from "./patientsSelectors";
import { FORM_NAME } from "./PatientForm";

const PATIENT_CREATE_STATES = {
  idle: "idle",
  dataFilling: "dataFilling",
};

const PATIENT_UPDATE_STATES = {
  idle: "idle",
  dataFilling: "dataFilling",
};

function PatientsPage() {
  const [patientCreateProcess, changePatientCreateProcess] = useState(
    PATIENT_CREATE_STATES.idle
  );
  const [patientUpdateProcess, changePatientUpdateProcess] = useState(
    PATIENT_UPDATE_STATES.idle
  );
  const dispatch = useDispatch();

  const allPatients = useSelector(allPatientsSelector);
  const allPatientsRequestState = useSelector(allPatientsRequestStateSelector);
  const createPatientRequestState = useSelector(
    createPatientRequestStateSelector
  );
  const deletePatientRequestState = useSelector(
    deletePatientRequestStateSelector
  );
  const updatePatientRequestState = useSelector(
    updatePatientRequestStateSelector
  );

  useEffect(() => {
    const init = async () => {
      await dispatch(getPatients());
    };

    init();
  }, [dispatch]);

  const startCreatePatientProcess = useCallback(() => {
    changePatientCreateProcess(PATIENT_CREATE_STATES.dataFilling);
  }, []);

  const startUpdatePatientProcess = useCallback(
    (patient) => () => {
      dispatch(initialize(FORM_NAME, patient));
      changePatientUpdateProcess(PATIENT_UPDATE_STATES.dataFilling);
    },
    [dispatch]
  );

  const handleCloseModal = useCallback(() => {
    changePatientUpdateProcess(PATIENT_UPDATE_STATES.idle);
    changePatientCreateProcess(PATIENT_CREATE_STATES.idle);
  }, []);

  const handleCreatePatient = useCallback(
    async (values) => {
      try {
        await dispatch(createPatient(values));
        await dispatch(getPatients());
        changePatientCreateProcess(PATIENT_CREATE_STATES.idle);
      } catch (err) {
        console.log(err);

        throw new SubmissionError({
          _error: _.get(err, ["response", "data", "errors"]),
        });
      }
    },
    [dispatch]
  );

  const handleUpdatePatient = useCallback(
    async (values) => {
      const { id } = values;

      try {
        await dispatch(updatePatient(id, values));
        await dispatch(getPatients());
        changePatientUpdateProcess(PATIENT_UPDATE_STATES.idle);
      } catch (err) {
        console.log(err);

        throw new SubmissionError({
          _error: _.get(err, ["response", "data", "errors"]),
        });
      }
    },
    [dispatch]
  );

  const handleDeletePatient = useCallback(
    async (id) => {
      try {
        await dispatch(deletePatient(id));
        await dispatch(getPatients());
      } catch (err) {
        console.log(err);

        throw new SubmissionError({
          _error: _.get(err, ["response", "data", "errors"]),
        });
      }
    },
    [dispatch]
  );

  const showPreloader = useMemo(() => {
    return (
      allPatientsRequestState === "requested" ||
      deletePatientRequestState === "requested"
    );
  }, [allPatientsRequestState, deletePatientRequestState]);

  const showError = useMemo(() => {
    return allPatientsRequestState === "failed";
  }, [allPatientsRequestState]);

  const showPatientsList = useMemo(() => {
    return allPatientsRequestState === "success";
  }, [allPatientsRequestState]);

  return (
    <Layout>
      <Container>
        {showPreloader && "Загрузка..."}

        {showError && "Произошла ошибка при загрузке данных"}

        {showPatientsList && (
          <>
            <PatientList
              data={allPatients}
              onCreate={startCreatePatientProcess}
              onUpdate={startUpdatePatientProcess}
              onDelete={handleDeletePatient}
            />
            {patientCreateProcess === PATIENT_CREATE_STATES.dataFilling && (
              <PatientModal
                processName="patientCreate"
                onOk={handleCreatePatient}
                onClose={handleCloseModal}
                isSubmitting={createPatientRequestState === "requested"}
              />
            )}
            {patientUpdateProcess === PATIENT_UPDATE_STATES.dataFilling && (
              <PatientModal
                processName="patientUpdate"
                onOk={handleUpdatePatient}
                onClose={handleCloseModal}
                isSubmitting={updatePatientRequestState === "requested"}
              />
            )}
          </>
        )}
      </Container>
    </Layout>
  );
}

export default PatientsPage;
